package ${package}.dto;

import lombok.Data;

@Data
public class ServiceInfo {

    String name;
    String description;

}
