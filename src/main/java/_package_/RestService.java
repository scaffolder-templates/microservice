package ${package};

import ${package}.dto.ServiceInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/service")
public class RestService {

    @GetMapping("/{id}/name")
    public String getName() {
        return null;
    }

    @GetMapping("/{id}/info")
    public ServiceInfo getInfo() {
        return null;
    }

}
